from flask import Flask
application = Flask(__name__)

@application.route("/")
def hello():
    return "Hello Deploy!"
    
@application.route("/hoge")
def hello2():
    return "Hello Hoge!"

if __name__ == "__main__":
    application.run()
